#!/bin/bash
# Use: ggthumb input_file output_file size

if [ "$#" -ne "3" ] ; then
	echo "Usage: ggthumb input_file output_file size"
	exit 1
fi

INPUT_FILE="$1"
OUTPUT_FILE="$2"
SIZE="$3"

TEMPDIR="`mktemp -d`"
unzip -qq "$INPUT_FILE" -d "$TEMPDIR"
convert -resize "$SIZE" "$TEMPDIR/geogebra_thumbnail.png" "$OUTPUT_FILE"
rm -fr "$TEMPDIR"

