geogebra for Debian
-------------------

The upstream source tarball is obtained by the script
get_orig_source.sh by exporing a copy of the SVN tag corresponding to
the desired version and then:

 * Deleting the scripts/, installer/ and webstart/ directory, which
   are not necessary for the Debian package (and possibily contain
   non-free pieces);

 * Deleteing many embedded copies of other pieces of software,
   documented below.


QUILT

This package uses quilt to manage all modifications to the upstream
source.  Changes are stored in the source package as diffs in
debian/patches and applied during the build.  Please see:

    /usr/share/doc/quilt/README.source

for more information on how to apply the patches, modify patches, or
remove a patch.


EMBEDDED SOFTWARE

The following pieces of software are present in the upstream
distribution of GeoGebra, but are not included in the Debian package:

 * JavaCC, that is already included in Debian;

 * Some Apache Commons libraries, that are already included in Debian
   (org/apache/commmons/);

 * The Apache Log4J library, that is already included in Debian
   (org/apache/log4j/);

 * The FreeHEP libraries, that are already included in Debian
   (org/freehep/);

 * A few JDesktop classes, which appear to be unnecessary and are
   anyway already included in package libswing-layout-java
   (org/jdesktop/);

 * JFugue, that is already included in Debian (org/jfugue/);

 * Mathpiper, that is already included in Debian (org/mathpiper/);

 * Rhino (or at least some pieces of it), a JavaScript implementation
   written in Java, which is already contained in _many_ Debian
   packages (the main one appears to be librhino-java);

 * Java Algebra System (JAS), that is already included in Debian
   (edu/jas/);

 * A file in the edu/mas/ directory, that appears to be unnecessary;

 * Part of the Java Universal Network/Graph Framework (JUNG), that is
   not in Debian at the moment (edu/uci/, sources from
   http://jung.sourceforge.net/); actually, the required classes are
   only a small part of JUNG: since maintaining it all in a separate
   package would be quite a burden and it would bring in dependencies
   that appear to be non-free, only some necessary classes are
   retained in the geogebra package;

 * JLaTeXMath, that is already included in Debian (jlatexmath.jar,
   jlm_cyrillic.jar, jlm_greek.jar);

 * Some other minor JAR archives, not necessary for compiling the
   Debian pacakge.
