Source: geogebra
Section: education
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Giovanni Mascellani <gio@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 javahelper (>= 0.38),
 javacc,
 default-jdk,
 xsltproc,
 docbook-xsl,
 mathpiper (>= 0.81f+svn4469+dfsg3),
 libcommons-collections3-java,
 libcommons-math-java,
 libfreehep-xml-java,
 libfreehep-util-java,
 libfreehep-io-java,
 libfreehep-graphics2d-java (>= 2.4),
 libfreehep-graphicsio-java (>= 2.4),
 libfreehep-graphicsio-svg-java (>= 2.4),
 libfreehep-graphicsio-pdf-java (>= 2.4),
 libfreehep-graphicsio-ps-java (>= 2.4),
 libfreehep-graphicsio-emf-java (>= 2.4),
 libjfugue-java,
 libjlatexmath-java,
 librhino-java (>= 1.7R3-5)
Standards-Version: 4.5.1
Homepage: https://www.geogebra.org
Vcs-Browser: https://salsa.debian.org/java-team/geogebra
Vcs-Git: https://salsa.debian.org/java-team/geogebra.git
Rules-Requires-Root: no

Package: geogebra
Architecture: all
Depends:
 ${java:Depends}, ${misc:Depends},
 mathpiper (>= 0.81f+svn4469+dfsg3),
 libfreehep-graphicsio-svg-java (>= 2.4),
 libfreehep-graphicsio-pdf-java (>= 2.4),
 libfreehep-graphicsio-ps-java (>= 2.4),
 libfreehep-graphicsio-emf-java (>= 2.4),
 librhino-java (>= 1.7R3-5)
Suggests: cups
Description: Dynamic mathematics software for education
 GeoGebra is a dynamic geometry program. You can do constructions with points,
 vectors, segments, lines, conic sections as well as functions and change
 them dynamically afterwards. On the other hand, equations and coordinates
 can be entered directly.
 .
 Support for many geometric constructions is provided, as well as support
 for many calculus-based tools (derivatives, osculating circle, ...).
 .
 GeoGebra files can be exported in many different formats, or as interactive
 applets for web pages.

Package: geogebra-gnome
Architecture: all
Depends:
 geogebra,
 imagemagick,
 ${misc:Depends}
Description: GNOME integration layer for GeoGebra
 GeoGebra is a dynamic geometry system. You can do constructions with points,
 vectors, segments, lines, conic sections as well as functions and change
 them dynamically afterwards. On the other hand, equations and coordinates
 can be entered directly.
 .
 Support for many geometric constructions is provided, as well as support
 for many elementary calculus-based tools (derivatives, osculating circle, ...).
 .
 GeoGebra files can be exported in many different formats, or as interactive
 applets for web pages.
 .
 This package contains the GNOME thumbnailer for the GeoGebra file format.
