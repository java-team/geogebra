<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- README: Building GeoGebra 

This script builds several kinds of GeoGebra *.jar files,
primarily for creating an official build for GeoGebra.
It must be run using JDK 1.5 (NOT 1.6 or later).
Note that GeoGebra itself needs to be compiled using Java 1.5

To learn more on building GeoGebra by using this script, please
read the following web pages:

  http://www.geogebra.org/trac/wiki/SetUpCommandLine
  http://www.geogebra.org/trac/wiki/BuildingProcess

If you want to include GeoGebra with e.g. Linux distributions or
other software, please see the license conditions below and contact
us at office@geogebra.org. We will try our best to provide installers
for various platforms. However, we cannot provide our signature files
to create signed *.jar files.

LICENSE
Please note that GeoGebra's source code is licensed under GNU GPL but
all GeoGebra language files (geogebra/properties) bundled in the file
geogebra_properties.jar are subject to a Creative Commons Attribution-
Share Alike license (see LICENSE.txt).

If you would like to contribute to GeoGebra in any way, please let us
know. Your help is always welcome!

The GeoGebra Team
office@geogebra.org 
-->

<!--
   GeoGebra build script
   @author Markus Hohenwarter
-->

<project default="geogebra">

	<!-- GENERAL properties (equal for all os, platforms, users, ...) -->
	<property name="local.property.missing"
		location="A build-local property was not specified. Did you create 'build-local.properties'?"/>
	<property name="src.dir" location="."/>
	<property name="propertiessrc.dir" location="./geogebra/properties"/>
	<property name="build.dir" location="build"/>
	<property name="propertiestemp.dir" location="${build.dir}/tempproperties"/>
	<property name="temp.dir" location="${build.dir}/temp"/>
	<property name="workspace.dir" location="../"/>
	<!--property environment="env"/-->
	
	<!-- USER-SPECIFIC properties -->
	<!-- Include properties set in [build-local.properties] -->
	<property file="build-local.properties"/>
	<!-- These properties must be specified in [build-local.properties] -->
	<fail unless="javacc.home" message="${local.property.missing}"/>
	

	<!-- build signed and unsigned GeoGebra jar files -->
	<target name="geogebra" 
		depends="clean, ggb-jar-files, copyDebug, signDebug, obfuscate, ggb-jar-index, 
					copyJars, sign, pack, preloadHtml, finish">
	</target>

	<!-- build signed and unsigned GeoGebra jar files (from command line) -->
	<target name="geogebra-cl"
		depends="clean, compile-grammar-cl, compile-oe-cl, geogebra">
	</target>

	<!-- build jar files for testing without obfuscation -->
	<target name="geogebra-unobfuscated" 
		depends="clean, ggb-jar-files, ggb-jar-index, 
					copyJars, sign, pack, preloadHtml, finish">
	</target>

	<!-- create all jar files -->
	<target name="ggb-jar-files" 
			depends="ggb-jar, ggb-main-jar, ggb-export-jar, ggb-properties-jar, ggb-cas-jar, ggb-algos-jar, ggb-gui-jar, ggb-javascript-jar, ggb-copy-jlatexmath">
	</target>

	<!-- compiles grammar -->
	<target name="compile-grammar">
		<javacc
		   target="./geogebra/kernel/parser/Parser.jj"
		   javacchome="${javacc.home}"
		   static="false"
		/>
	</target>

	<target name="compile-oe">
		<mkdir dir="${build.dir}/classes" />
		<path id="build.class.path">
			<fileset dir="${src.dir}">
				<include name="*.jar" />
			</fileset>
		</path>
		<javac srcdir="${src.dir}" destdir="${build.dir}/classes">
			<classpath refid="build.class.path" />
		<!-- Adding line numbers -->
		<compilerarg value="-g" />
		</javac>
	</target>

	<!-- compiles grammar for cl -->
	<target name="compile-grammar-cl" depends="compile-grammar-clean, compile-grammar">
	</target>

	<target name="compile-grammar-clean">
		<delete>
			<fileset dir="./geogebra/kernel" includes="**/*.class" />
		</delete>
		<delete>
			<fileset dir="./geogebra/kernel/parser" includes="*.java"/>
		</delete>
	</target>

	<target name="delete-sources">
		<delete>
			<fileset dir="." includes="**/*.java" />
		</delete>
		<delete>
			<fileset dir="." includes="geogebra/properties/*.properties" />
		</delete>
	</target>

	<target name="delete-classes">
		<delete>
			<fileset dir="." includes="**/*.class" />
		</delete>
	</target>

	<target name="compile-oe-cl">
		<path id="build.class.path">
			<fileset dir="${src.dir}">
				<include name="*.jar"/>
			</fileset>
		</path>
		<javac srcdir="${src.dir}">
			<classpath refid="build.class.path"/>
			<!-- Adding line numbers -->
			<compilerarg value="-g" />
		</javac>
	</target>

	<manifest file="../manifest.mf">
		<attribute name="Main-Class" value="geogebra.GeoGebra"/>
		<attribute name="Class-Path" 
			value="geogebra.jar geogebra_main.jar geogebra_gui.jar geogebra_properties.jar geogebra_export.jar geogebra_cas.jar geogebra_algos.jar geogebra_javascript.jar jlatexmath.jar jlm_greek.jar jlm_cyrillic.jar"/>
		<!--	<attribute name="SplashScreen-Image" value="geogebra/splash.gif"/>	-->
	</manifest>

	<target name="clean">
		<!-- delete build directory -->
		<mkdir dir="${build.dir}"/>
		<delete includeemptydirs="true">
			<fileset dir="${build.dir}" defaultexcludes="false">
				<include name="**/*" />
			</fileset>
		</delete>

		<mkdir dir="${build.dir}"/>
		<mkdir dir="${build.dir}/packed"/>
		<mkdir dir="${build.dir}/unpacked"/>
		<mkdir dir="${build.dir}/unsigned"/>
		<mkdir dir="${build.dir}/debug"/>
		<mkdir dir="${build.dir}/unsigned/packed"/>
		<mkdir dir="${build.dir}/unsigned/unpacked"/>
		<mkdir dir="${propertiestemp.dir}"/>
	</target>

	<target name="finish">
		<delete dir="${propertiestemp.dir}"/>
	</target>

	<!-- remove comments from properties files -->
	<target name="stripComments">
		<copy todir="${propertiestemp.dir}/geogebra/properties" encoding="ISO-8859-1">
			<fileset dir="${propertiessrc.dir}">
				<include name="*" />
			</fileset>
			<filterchain>
				<trim/>
				<striplinecomments>
					<comment value="#"/>
				</striplinecomments>
			</filterchain>
		</copy>
	</target>

	<!-- put jar files into subdirectories signed and unsigned -->
	<target name="copyJars" depends="ggb-jar-files">
		<copy todir="${build.dir}/unsigned/unpacked">
			<fileset dir="${build.dir}">
				<include name="*.jar" />
			</fileset>
		</copy>
	</target>

	<!-- put jar files (unobfuscated, unindexed) into subdirectory debug -->
	<target name="copyDebug" depends="ggb-jar-files">
		<copy todir="${build.dir}/debug">
			<fileset dir="${build.dir}">
				<include name="*.jar" />
			</fileset>
		</copy>
	</target>

	<!-- geogebra.jar loads the geogebra_main.jar file and starts up the application/applet  -->
	<target name="ggb-jar" depends="clean" description="create unsigned geogebra.jar">
		<jar jarfile="${build.dir}/geogebra.jar" manifest="../manifest.mf" >
			<fileset dir="${src.dir}"		   
				includes="geogebra/*"			
				excludes="**/*.java"/>
		</jar>
	</target>

	<!-- create index.list in geogebra.jar for optimized loading of other jars by Java classloader -->
	<target name="ggb-jar-index" depends="ggb-jar-files" description="create index in geogegebra.jar">
		<exec executable="jar" dir="${build.dir}">
			<arg value="i"/>
			<arg value="geogebra.jar"/>
			<arg value="geogebra_main.jar"/>
			<arg value="geogebra_gui.jar"/>
			<arg value="geogebra_cas.jar"/>
			<arg value="geogebra_algos.jar"/>
			<arg value="geogebra_export.jar"/>
			<arg value="geogebra_javascript.jar"/>
			<arg value="geogebra_properties.jar"/>
			<arg value="jlatexmath.jar"/>
			<arg value="jlm_cyrillic.jar"/>
			<arg value="jlm_greek.jar"/>
		</exec>
	</target>

	<!-- copies the jlatexmath jars  -->
	<target name="ggb-copy-jlatexmath" depends="clean" description="copy jlatexmath jars">
		<copy file="${src.dir}/jlatexmath.jar"
		      tofile="${build.dir}/jlatexmath.jar">
		</copy>
		<copy file="${src.dir}/jlm_greek.jar"
		      tofile="${build.dir}/jlm_greek.jar">
		</copy>
		<copy file="${src.dir}/jlm_cyrillic.jar"
		      tofile="${build.dir}/jlm_cyrillic.jar">
		</copy>
	</target>


	<!-- geogebra_main.jar includes all basic classes to run the application and applet  -->
	<target name="ggb-main-jar" depends="clean" description="create unsigned geogebra_main.jar">
		<jar jarfile="${build.dir}/geogebra_main.jar"		
			basedir="${src.dir}"		   
			includes="**/*.class,	
		  			geogebra/main/default-preferences.xml, 				
					  geogebra/main/*.png,
			  edu/xtec/adapter/**,
			  edu/xtec/adapter/impl/**,
					  **/algo2command.properties,	
					  **/algo2intergeo.properties"			
			excludes="geogebra/*,
					geogebra/gui/**,
					geogebra/export/**, 
					geogebra/cas/**,
			geogebra/kernel/discrete/**,
			geogebra/kernel/cas/**,
			geogebra/sound/**,
					jasymca/**,		
					org/**,
					org/mathpiper/**,
					org/mozilla/**,
					org/freehep/**, 
					org/concord/**,
					ccsd/**,
					geogebra/usb/**,
					edu/mas/**,
					edu/jas/**,
					edu/uci/**,
					org/apache/**,
					org/apache/log4j/**, 
			com/bric/**,
				    tutor/**,
					meta-inf/**"		
		/>
	</target>

	<target name="ggb-export-jar" depends="clean" description="create unsigned geogebra_export.jar">
		<jar jarfile="${build.dir}/geogebra_export.jar"
			basedir="${src.dir}"
			includes="geogebra/export/**, 
					org/freehep/**, 
					META-INF/services/**"
			excludes="**/*.java,
			**/*.html" 
		/>
	</target>

	<target name="ggb-javascript-jar" depends="clean" description="create unsigned geogebra_javascript.jar">
		<jar jarfile="${build.dir}/geogebra_javascript.jar"
			basedir="${src.dir}"
			includes="org/mozilla/**"
			excludes="**/*.java" 
		/>
	</target>

	<target name="ggb-cas-jar" depends="clean" description="create unsigned geogebra_cas.jar">
		<jar jarfile="${build.dir}/geogebra_cas.jar"
			basedir="${src.dir}"
			includes="geogebra/cas/**,
					org/apache/log4j/**,
					org/mathpiper/mpreduce/**,
			geogebra/kernel/cas/**,
					default.img"
			excludes="**/*.java,
			geogebra/cas/maxima/**,
			geogebra/cas/jacomax/**,
			geogebra/cas/mathpiper/**" 
		/>
	</target>

	<target name="ggb-algos-jar" depends="clean" description="create unsigned geogebra_algos.jar">
		<jar jarfile="${build.dir}/geogebra_algos.jar"
			basedir="${src.dir}"
			includes="geogebra/kernel/discrete/**,
			geogebra/sound/**,
					edu/uci/**,
					org/jfugue/**,
					org/apache/**"
			excludes="**/*.java,
			**/*.html,
			**/*.txt,
			org/apache/log4j/**" 
		/>
	</target>

	<target name="ggb-gui-jar" depends="clean" description="create unsigned geogebra_gui.jar">
		<jar jarfile="${build.dir}/geogebra_gui.jar"
			basedir="${src.dir}"
			includes="geogebra/gui/**"
			excludes="**/*.java" 
		/>
	</target>

	<target name="ggb-properties-jar" depends="clean, stripComments" description="create unsigned geogebra_properties.jar">
		<jar jarfile="${build.dir}/geogebra_properties.jar"
			basedir="${propertiestemp.dir}"
			includes="**/*"		
			excludes="**/wiki*.*"
		/>
	</target>


	<!-- Define Proguard task -->
	<taskdef 
		resource="proguard/ant/task.properties" 
		classpath="tools_proguard46.jar" />

	<!-- Define pack200 task -->
	<!-- bug in Java 6, see http://www.geogebra.org/forum/viewtopic.php?f=8&t=3972&st=0&sk=t&sd=a&start=15 -->
	<taskdef name="p200ant"
	    classname="de.matthiasmann.p200ant.P200AntTask"
	    classpath="tools_p200ant_java5only.jar"/>


	<!-- Obfuscate jar files without signing -->
	<target name="obfuscate" depends="ggb-jar-files">
		<!-- check if java150-rt.jar file present -->
		<condition property="java150-rt.present">
			<available file="${workspace.dir}/java150-rt.jar"/>
		</condition>
		<antcall target="doObfuscate"/>
	</target>

	<target name="doObfuscate" if="java150-rt.present" description="obfuscate jar files">
		<proguard configuration="build.pro"/>
		<move todir="${build.dir}">
			<fileset dir="${temp.dir}">
				<include name="*.jar"/>
			</fileset>
		</move>
		<delete dir="${temp.dir}"/>
	</target>

	<!-- Sign jar files -->
	<target name="sign" description="repack and sign jar files twice (to ensure WebStart compatibility)">
		<!-- check if keystore file present -->
		<condition property="keystore.present">
			<available file="${workspace.dir}/igi-keystore.p12"/>
		</condition>

		<!-- repack and sign jars first time -->
		<antcall target="repack"/>
		<antcall target="doSign"/>

		<!-- used to use to repack and sign jars second time -->
		<!-- not needed  due to pack.segment.limit=-1 works properly -->
		<!--antcall target="repack"/-->
		<!--antcall target="doSign"/-->
	</target>

	<!-- sign jars -->
	<target name="doSign" if="keystore.present" description="sign jar files">
		<signjar 
			keystore="${workspace.dir}/igi-keystore.p12" 	
			alias="International GeoGebra Institute&apos;s GlobalSign nv-sa ID"
			storetype="pkcs12"
			storepass="geogebra">
			<fileset dir="${build.dir}" includes="*.jar" />
		</signjar>
	</target>

	<!-- Sign debug jar files -->
	<target name="signDebug" description="repack and sign jar files twice (to ensure WebStart compatibility)">
		<!-- check if keystore file present -->
		<condition property="keystore.present">
			<available file="${workspace.dir}/igi-keystore.p12"/>
		</condition>

		<antcall target="doSignDebug"/>

	</target>

	<!-- sign debug jars -->
	<target name="doSignDebug" if="keystore.present" description="sign jar files">
		<signjar 
			keystore="${workspace.dir}/igi-keystore.p12" 	
			alias="International GeoGebra Institute&apos;s GlobalSign nv-sa ID"
			storetype="pkcs12"
			storepass="geogebra">
			<fileset dir="${build.dir}/debug" includes="*.jar" />
		</signjar>
	</target>

	<!-- repack jars -->
	<target name="repack" if="keystore.present" description="repack jar files in preparation for signing">
                <fileset id="jars2pack" dir="${build.dir}">
                        <include name="*.jar"/>
                </fileset>
                <p200ant repack="true"  configFile="tools_p200.config">
                        <fileset refid="jars2pack"/>
                </p200ant>
	</target>

	<!-- pack jars -->
	<!-- This should prevent running the pack target on Java 1.6
	     http://www.java-tips.org/other-api-tips/ant/how-can-i-test-for-jvm-versions.html -->
  	<target name="get-jvm">
      		<condition property="jvm.buggy">
			<or>
      				<equals arg1="${ant.java.version}" arg2="1.6"/>
      			</or>
      		</condition>
      	</target>

	<target name="pack" description="pack jar files" depends="get-jvm" unless="jvm.buggy">
                <fileset id="unsignedJars" dir="${build.dir}/unsigned/unpacked">
                        <include name="*.jar"/>
                </fileset>
                <p200ant destdir="${build.dir}/unsigned/packed"  configFile="tools_p200.config">
                        <fileset refid="unsignedJars"/>
                </p200ant>

		<move todir="${build.dir}/unpacked">
			<fileset dir="${build.dir}">
				<include name="*.jar" />
			</fileset>
		</move>

                <fileset id="signedJars" dir="${build.dir}/unpacked">
                        <include name="*.jar"/>
                </fileset>
                <p200ant destdir="${build.dir}/packed"  configFile="tools_p200.config">
                        <fileset refid="signedJars"/>
                </p200ant>

	</target>

	<!-- reads the version/build number from geogebra.GeoGebra.java -->
	<target name="readVersion">
		<loadfile property="fullversion" srcfile="${src.dir}/geogebra/GeoGebra.java">
			<filterchain>
				<linecontainsregexp>
					<regexp pattern="public static final String VERSION_STRING.*=" />
				</linecontainsregexp>
				<containsregex pattern=".*&quot;(.*)&quot;.*" replace="\1"/>
				<deletecharacters chars="\r\n" />
			</filterchain>
		</loadfile>
		<loadfile property="version" srcfile="${src.dir}/geogebra/GeoGebra.java">
			<filterchain>
				<linecontainsregexp>
					<regexp pattern="public static final String VERSION_STRING.*=" />
				</linecontainsregexp>
				<containsregex pattern=".*&quot;(.*)\..*&quot;.*" replace="\1"/>
				<deletecharacters chars="\r\n" />
			</filterchain>
		</loadfile>
		<loadfile property="build" srcfile="${src.dir}/geogebra/GeoGebra.java">
			<filterchain>
				<linecontainsregexp>
					<regexp pattern="public static final String VERSION_STRING.*=" />
				</linecontainsregexp>
				<containsregex pattern=".*&quot;.*\..*\..*\.(.*)&quot;.*" replace="\1"/>
				<deletecharacters chars="\r\n" />
			</filterchain>
		</loadfile>
		<loadfile property="versionname" srcfile="${src.dir}/geogebra/GeoGebra.java">
			<filterchain>
				<linecontainsregexp>
					<regexp pattern="public static final String VERSION_STRING.*=" />
				</linecontainsregexp>
				<containsregex pattern=".*&quot;(.*)&quot;.*" replace="\1"/>
				<deletecharacters chars="\r\n" />
				<tokenfilter>
					<replaceregex pattern="\." replace="-" flags="g" />
				</tokenfilter>
			</filterchain>
		</loadfile>
		<!--
		<echo> ${fullversion} </echo>
		<echo> ${version}     </echo>
		<echo> ${build}       </echo>
		<echo> ${versionname} </echo>
		-->
	</target>

	<!-- creates preload.html by inserting version strings into preload.txt -->
	<target name="preloadHtml" depends="readVersion">
		<copy file="${src.dir}/preload.txt"
		      tofile="${build.dir}/unsigned/preload.html">
		</copy>
		<replace file="${build.dir}/unsigned/preload.html" 
			token="VERSION_STRING" value="${fullversion}"/>
		<copy file="${src.dir}/version.txt"
		      tofile="${build.dir}/unpacked/version.txt">
		</copy>
		<replace file="${build.dir}/unpacked/version.txt" 
			token="VERSION_STRING" value="${fullversion}"/>
	</target>

	<target name="installer" depends="installer-clean, installer-windows, installer-macosx"/>

	<target name="installer-clean">
		<delete dir="${build.dir}/installer"/>
		<mkdir  dir="${build.dir}/installer"/>
		<mkdir  dir="${build.dir}/installer/windows"/>
		<mkdir  dir="${build.dir}/installer/windows/unsigned"/>
		<mkdir  dir="${build.dir}/installer/windows/temp"/>
		<mkdir  dir="${build.dir}/installer/macosx"/>
	</target>

	<!-- defines the launch4j and nsis tasks used to create the Windows installer -->
	<taskdef name="launch4j" classname="net.sf.launch4j.ant.Launch4jTask" classpath="installer/launch4j/launch4j.jar" />
	<taskdef name="nsis" classname="net.sf.nsisant.Task" classpath="installer/nsisant-1.2.jar" />

	<!-- defines the conditions used during creation of the Windows installer -->
	<condition property="installer-windows">
		<or>
			<os family="winnt"/>
			<and>
				<os family="mac"/>
				<available file="/Applications/Wine.app"/>
			</and>
			<and>
				<os family="unix"/>
				<available file="/usr/bin/wine"/>
			</and>
		</or>
	</condition>
	<condition property="installer-windows-keystore">
			<or>
				<and>
					<os family="winnt"/>
					<available file="${workspace.dir}/igi-keystore.pvk"/>
					<available file="${workspace.dir}/igi-keystore.spc"/>
				</and>
				<and>
					<os family="mac"/>
					<available file="${workspace.dir}/igi-keystore.der"/>
					<available file="${workspace.dir}/igi-keystore.spc"/>
				</and>
				<and>
					<os family="unix"/>
					<available file="${workspace.dir}/igi-keystore-CL.spc.der"/>
				</and>
			</or>
	</condition>
	<condition property="installer-windows-uninstaller" value="${build.dir}\installer\windows\unsigned\">
		<os family="winnt"/>
	</condition>
	<condition property="installer-windows-uninstaller" value="C:\">
		<os family="mac"/>
	</condition>
	<condition property="installer-windows-uninstaller" value="${build.dir}\installer\windows\unsigned\">
		<os family="unix"/>
	</condition>
	<condition property="installer-windows-osfamily-mac">
		<os family="mac"/>
	</condition>
	<condition property="installer-windows-osfamily-unix">
		<os family="unix"/>
	</condition>

	<target name="GeoGebra.exe">
		<!--nsis script="installer/GeoGebra_exe.nsi" verbosity="1" path="${src.dir}/installer/nsis">
		</nsis-->
		<launch4j configFile="${src.dir}/installer/geogebra.xml"
			outfile="${build.dir}/installer/windows/unsigned/GeoGebra.exe"
			fileVersion="${fullversion}"
			txtFileVersion="${fullversion}"
			productVersion="${fullversion}"
			txtProductVersion="${fullversion}"/>
	<copy file="${build.dir}/installer/windows/unsigned/GeoGebra.exe" tofile="${build.dir}/installer/windows/GeoGebra.exe"/>
	</target>

	<target name="GeoGebraPrim.exe">
		<!--nsis script="installer/GeoGebraPrim_exe.nsi" verbosity="1" path="${src.dir}/installer/nsis">
		</nsis-->
		<launch4j configFile="${src.dir}/installer/geogebraprim.xml"
			outfile="${build.dir}/installer/windows/unsigned/GeoGebraPrim.exe"
			fileVersion="${fullversion}"
			txtFileVersion="${fullversion}"
			productVersion="${fullversion}"
			txtProductVersion="${fullversion}"/>
	<copy file="${build.dir}/installer/windows/unsigned/GeoGebraPrim.exe" tofile="${build.dir}/installer/windows/GeoGebraPrim.exe"/>
	</target>
	
	<!-- creates a GeoGebra installer for Windows -->
	<target name="installer-windows" depends="readVersion, GeoGebra.exe, GeoGebraPrim.exe" if="installer-windows">
		<tstamp />
		<nsis script="installer/geogebra.nsi" verbosity="1" path="${src.dir}/installer/nsis">
			<define name="build.dir"   value="${build.dir}"/>
			<define name="fullversion" value="${fullversion}"/>
			<define name="versionname" value="${versionname}"/>
			<define name="builddate"   value="${TODAY}"/>
			<define name="outfile"     value="${build.dir}/installer/windows/temp/uninstaller.exe"/>
			<define name="uninstaller" value="${installer-windows-uninstaller}"/>
		</nsis>
		<antcall target="installer-windows-sign-components"/>
		<exec executable="${build.dir}/installer/windows/temp/uninstaller.exe" osfamily="winnt"/>
		<exec dir="/Applications/Wine.app/Contents/MacOS" executable="/Applications/Wine.app/Contents/MacOS/startwine" osfamily="mac">
			<arg file="${build.dir}/installer/windows/temp/uninstaller.exe"/>
			<env key="WINEPREFIX" value="${workspace.dir}/wine"/>
		</exec>
		<exec executable="/usr/bin/wine" osfamily="unix">
			<arg file="${build.dir}/installer/windows/temp/uninstaller.exe"/>
			<env key="WINEPREFIX" value="${workspace.dir}/wine"/>
		</exec>
		<antcall target="installer-windows-move-uninstaller-mac"/>
		<antcall target="installer-windows-move-uninstaller-unix"/>
		<copy file="${build.dir}/installer/windows/unsigned/uninstaller.exe" tofile="${build.dir}/installer/windows/uninstaller.exe"/>
		<nsis script="installer/geogebra.nsi" verbosity="1" path="${src.dir}/installer/nsis">
			<define name="build.dir"   value="${build.dir}"/>
			<define name="fullversion" value="${fullversion}"/>
			<define name="versionname" value="${versionname}"/>
			<define name="builddate"   value="${TODAY}"/>
			<define name="outfile"     value="${build.dir}/installer/windows/unsigned/GeoGebra-Windows-Installer-${versionname}.exe"/>
		</nsis>
		<copy file="${build.dir}/installer/windows/unsigned/GeoGebra-Windows-Installer-${versionname}.exe" tofile="${build.dir}/installer/windows/GeoGebra-Windows-Installer-${versionname}.exe"/>
		<antcall target="installer-windows-sign-installer"/>
		<move file="${build.dir}/installer/windows/GeoGebra-Windows-Installer-${versionname}.exe" tofile="${build.dir}/installer/GeoGebra-Windows-Installer-${versionname}.exe"/>
		<!--delete dir="${build.dir}/installer/windows" followsymlinks="false" removeNotFollowedSymlinks="true"/-->
	</target>
	<target name="installer-windows-move-uninstaller-mac" if="installer-windows-osfamily-mac">
		<move file="${workspace.dir}/wine/drive_c/uninstaller.exe" tofile="${build.dir}/installer/windows/unsigned/uninstaller.exe"/>
	</target>
	<target name="installer-windows-move-uninstaller-unix" if="installer-windows-osfamily-unix">
		<move file="${build.dir}/installer/windows/temp/uninstaller.exe" tofile="${build.dir}/installer/windows/unsigned/uninstaller.exe"/>
	</target>
	<target name="installer-windows-sign-uninstall-generator" if="installer-windows-keystore">
		<exec executable="${src.dir}/installer/signcode" osfamily="winnt">
			<arg line="-spc '${workspace.dir}/igi-keystore.spc' -v '${workspace.dir}/igi-keystore.pvk' -n GeoGebra -i http://www.geogebra.org/ -t http://timestamp.verisign.com/scripts/timstamp.dll '${build.dir}/installer/windows/temp/uninstaller.exe'"/>
		</exec>
	</target>
	<target name="installer-windows-sign-components" if="installer-windows-keystore">
		<exec executable="${src.dir}/installer/signcode" osfamily="winnt">
			<arg line="-spc '${workspace.dir}/igi-keystore.spc' -v '${workspace.dir}/igi-keystore.pvk' -n GeoGebra -i http://www.geogebra.org/ -t http://timestamp.verisign.com/scripts/timstamp.dll '${build.dir}/installer/windows/GeoGebra.exe'"/>
		</exec>
		<exec executable="${src.dir}/installer/signcode" osfamily="winnt">
			<arg line="-spc '${workspace.dir}/igi-keystore.spc' -v '${workspace.dir}/igi-keystore.pvk' -n GeoGebra -i http://www.geogebra.org/ -t http://timestamp.verisign.com/scripts/timstamp.dll '${build.dir}/installer/windows/GeoGebraPrim.exe'"/>
		</exec>
		<exec executable="${src.dir}/installer/signcode" osfamily="winnt">
			<arg line="-spc '${workspace.dir}/igi-keystore.spc' -v '${workspace.dir}/igi-keystore.pvk' -n GeoGebra -i http://www.geogebra.org/ -t http://timestamp.verisign.com/scripts/timstamp.dll '${build.dir}/installer/windows/temp/uninstaller.exe'"/>
		</exec>
		<exec executable="${src.dir}/installer/osslsigncode" osfamily="mac">
			<arg line="-spc '${workspace.dir}/igi-keystore.spc' -key '${workspace.dir}/igi-keystore.der' -n GeoGebra -i http://www.geogebra.org/ -t http://timestamp.verisign.com/scripts/timstamp.dll -in '${build.dir}/installer/windows/unsigned/GeoGebra.exe' -out '${build.dir}/installer/windows/GeoGebra.exe'"/>
		</exec>
		<exec executable="${src.dir}/installer/osslsigncode" osfamily="mac">
			<arg line="-spc '${workspace.dir}/igi-keystore.spc' -key '${workspace.dir}/igi-keystore.der' -n GeoGebra -i http://www.geogebra.org/ -t http://timestamp.verisign.com/scripts/timstamp.dll -in '${build.dir}/installer/windows/unsigned/GeoGebraPrim.exe' -out '${build.dir}/installer/windows/GeoGebraPrim.exe'"/>
		</exec>
		<exec executable="${src.dir}/installer/osslsigncode" osfamily="mac">
			<arg line="-spc '${workspace.dir}/igi-keystore.spc' -key '${workspace.dir}/igi-keystore.der' -n GeoGebra -i http://www.geogebra.org/ -t http://timestamp.verisign.com/scripts/timstamp.dll -in '${build.dir}/installer/windows/temp/uninstaller.exe' -out '${build.dir}/installer/windows/uninstaller.exe'"/>
		</exec>
		<exec executable="${src.dir}/installer/osslsigncode" osfamily="unix">
			<arg line="-spc '${workspace.dir}/igi-keystore-CL.spc.der' -key '${workspace.dir}/igi-keystore-CL.key.der' -n GeoGebra -i http://www.geogebra.org/ -t http://timestamp.verisign.com/scripts/timstamp.dll -in '${build.dir}/installer/windows/unsigned/GeoGebra.exe' -out '${build.dir}/installer/windows/GeoGebra.exe'"/>
		</exec>
		<exec executable="${src.dir}/installer/osslsigncode" osfamily="unix">
			<arg line="-spc '${workspace.dir}/igi-keystore-CL.spc.der' -key '${workspace.dir}/igi-keystore-CL.key.der' -n GeoGebra -i http://www.geogebra.org/ -t http://timestamp.verisign.com/scripts/timstamp.dll -in '${build.dir}/installer/windows/unsigned/GeoGebraPrim.exe' -out '${build.dir}/installer/windows/GeoGebraPrim.exe'"/>
		</exec>
		<exec executable="${src.dir}/installer/osslsigncode" osfamily="unix">
			<arg line="-spc '${workspace.dir}/igi-keystore-CL.spc.der' -key '${workspace.dir}/igi-keystore-CL.key.der' -n GeoGebra -i http://www.geogebra.org/ -t http://timestamp.verisign.com/scripts/timstamp.dll -in '${build.dir}/installer/windows/temp/uninstaller.exe' -out '${build.dir}/installer/windows/uninstaller.exe'"/>
		</exec>
	</target>
	<target name="installer-windows-sign-installer" if="installer-windows-keystore">
		<exec executable="${src.dir}/installer/signcode" osfamily="winnt">
			<arg line="-spc '${workspace.dir}/igi-keystore.spc' -v '${workspace.dir}/igi-keystore.pvk' -n GeoGebra -i http://www.geogebra.org/ -t http://timestamp.verisign.com/scripts/timstamp.dll '${build.dir}/installer/windows/GeoGebra-Windows-Installer-${versionname}.exe'"/>
		</exec>
		<exec executable="${src.dir}/installer/osslsigncode" osfamily="mac">
			<arg line="-spc '${workspace.dir}/igi-keystore.spc' -key '${workspace.dir}/igi-keystore.der' -n GeoGebra -i http://www.geogebra.org/ -t http://timestamp.verisign.com/scripts/timstamp.dll -in '${build.dir}/installer/windows/unsigned/GeoGebra-Windows-Installer-${versionname}.exe' -out '${build.dir}/installer/windows/GeoGebra-Windows-Installer-${versionname}.exe'"/>
		</exec>
		<exec executable="${src.dir}/installer/osslsigncode" osfamily="unix">
			<arg line="-spc '${workspace.dir}/igi-keystore-CL.spc.der' -key '${workspace.dir}/igi-keystore-CL.key.der' -n GeoGebra -i http://www.geogebra.org/ -t http://timestamp.verisign.com/scripts/timstamp.dll -in '${build.dir}/installer/windows/unsigned/GeoGebra-Windows-Installer-${versionname}.exe' -out '${build.dir}/installer/windows/GeoGebra-Windows-Installer-${versionname}.exe'"/>
		</exec>
	</target>

	<!-- defines the jarbundler task used to create the Mac OS X bundle -->
	<taskdef name="jarbundler" classname="net.sourceforge.jarbundler.JarBundler" classpath="installer/jarbundler-2.1.0.jar" />

	<!-- creates a GeoGebra.app bundle for Mac OS X -->
	<target name="installer-macosx" depends="readVersion">
		<jarbundler
				dir="${build.dir}/installer/macosx"
				name="GeoGebra"
				mainclass="geogebra.GeoGebra" 
				jar="${build.dir}/unpacked/geogebra.jar"
				build="${build}"
				bundleid="geogebra.GeoGebra"
				icon="installer/geogebra.icns"
				infostring="GeoGebra ${fullversion}, (C) 2001-2011 International GeoGebra Institute"
				jvmversion="1.5.0+"
				shortname="GeoGebra"
				signature="GGB"
				stubfile="installer/JavaApplicationStub"
				version="${version}">
			<javaproperty name="apple.laf.useScreenMenuBar" value="true" />
			<javafileset dir="${build.dir}/unpacked" />
			<javafileset dir="${build.dir}/unsigned" includes="unpacked/*.jar" />
			<documenttype
				name="GeoGebra File"
				extensions="ggb"
				mimetypes="application/vnd.geogebra.file"
				iconfile="installer/geogebra.icns"
				role="Editor" />
			<documenttype
				name="GeoGebra Tool"
				extensions="ggt"
				mimetypes="application/vnd.geogebra.tool"
				iconfile="installer/geogebra.icns"
				role="Editor" />
		</jarbundler>
		<copy file="${src.dir}/installer/gpl-3.0.txt" tofile="${build.dir}/installer/macosx/GeoGebra.app/Contents/Resources/gpl-3.0.txt" />
		<move file="${build.dir}/installer/macosx/GeoGebra.app/Contents/Resources/Java/unpacked" tofile="${build.dir}/installer/macosx/GeoGebra.app/Contents/Resources/Java/unsigned" />
		<zip destfile="${build.dir}/installer/GeoGebra_${versionname}.zip">
			<zipfileset dir="${build.dir}/installer/macosx" excludes="GeoGebra.app/Contents/MacOS/JavaApplicationStub" />
			<zipfileset dir="${build.dir}/installer/macosx" includes="GeoGebra.app/Contents/MacOS/JavaApplicationStub" filemode="755" />
		</zip>
		<delete dir="${build.dir}/installer/macosx" followsymlinks="false"/>
	</target>
</project>
