package geogebra.gui.app;

import geogebra.CommandLineArguments;
import geogebra.main.Application;

import javax.swing.JFrame;

/**
 * Frame for geogebra 3D.
 * 
 * @author matthieu
 *
 */
public class GeoGebraFrame3D extends GeoGebraFrame {
	
	
	public static synchronized void main(CommandLineArguments args) {		
		GeoGebraFrame.init(args,new GeoGebraFrame3D());
	}
	
}
