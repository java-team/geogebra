/* 
GeoGebra - Dynamic Mathematics for Everyone
http://www.geogebra.org

This file is part of GeoGebra.

This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License as published by 
the Free Software Foundation.

*/

/*
 * AlgoDependentNumber.java
 *
 * Created on 30. August 2001, 21:37
 */

package geogebra.kernel;

import geogebra.euclidian.EuclidianConstants;
import geogebra.kernel.arithmetic.ExpressionNode;

/**
 *
 * @author  Markus
 * @version 
 */
public class AlgoDependentText extends AlgoElement {

	private ExpressionNode root;  // input
    private GeoText text;     // output              
        
    public AlgoDependentText(Construction cons, String label, ExpressionNode root) {
    	super(cons);
        this.root = root;  
        
       text = new GeoText(cons);
       setInputOutput(); // for AlgoElement
        
        // compute value of dependent number
        compute();      
        text.setLabel(label);
    }   
    
    public AlgoDependentText(Construction cons, ExpressionNode root) {
    	super(cons);
        this.root = root;  
        
       text = new GeoText(cons);
       setInputOutput(); // for AlgoElement
        
        // compute value of dependent number
        compute();
    }   
    
	public String getClassName() {
		return "AlgoDependentText";
	}
    
    public int getRelatedModeID() {
    	return EuclidianConstants.MODE_TEXT;
    }
	
    public ExpressionNode getRoot(){
    	return root;
    }
    
    
    // for AlgoElement
	protected void setInputOutput() {
        input = root.getGeoElementVariables();
        
        setOutputLength(1);        
        setOutput(0,text);        
        setDependencies(); // done by AlgoElement
    }    
    
    public GeoText getGeoText() { return text; }
    String oldFormat = "";
    // calc the current value of the arithmetic tree
    protected final void compute() {	
    	String format = text.getPrintDecimals()+","+text.getPrintFigures();
    	text.setTemporaryPrintAccuracy();
    	if(!format.equals(oldFormat)){
    		oldFormat = format;
    		for(int i=0;i<input.length;i++)
    			if(input[i].getParentAlgorithm()!=null &&
    					input[i].isGeoText() && !input[i].isLabelSet()){
    				input[i].setVisualStyle(text);
    				input[i].getParentAlgorithm().update();
    			}
    	}
    	
    	
    	try {    	
	    	boolean latex = text.isLaTeX();
	    	root.setHoldsLaTeXtext(latex);
	    	
	    	String str;
	    	if (latex) {
	    		str = root.evaluate().toLaTeXString(false);
	    	} else {
	    		str = root.evaluate().toValueString();
	    	}
	    	
	        text.setTextString(str);	        
	    } catch (Exception e) {
	    	text.setUndefined();
	    }
	    
	    text.restorePrintAccuracy();
    }   
    
    final public String toString() {
        // was defined as e.g.  text0 = "Radius: " + r
    	if (root == null) return "";
        return root.toString();
    }
    
    final public String toRealString() {
        // was defined as e.g.  text0 = "Radius: " + r
    	if (root == null) return "";
        return root.toRealString();
    }
}
